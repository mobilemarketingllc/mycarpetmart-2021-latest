<?php

namespace WPGO_Plugins\Simple_Sitemap;

/**
 * Enqueue plugin scripts.
 */
class Enqueue_Scripts
{
    /**
     * Common root paths/directories.
     *
     * @var $module_roots
     */
    protected  $module_roots ;
    /**
     * Main class constructor.
     *
     * @param Array  $module_roots Root plugin path/dir.
     * @param Object $utilities_fw An object of API utilities class.
     */
    public function __construct( $module_roots, $utilities_fw )
    {
        $this->module_roots = $module_roots;
        $this->utilities_fw = $utilities_fw;
        $this->plugin_version = get_plugin_data( $module_roots['file'] )['Version'];
        $this->enq_pfx = 'simple-sitemap';
        $this->plugin_settings_prefix = 'simple_sitemap';
        // Scripts for plugin settings page.
        add_action( 'admin_enqueue_scripts', array( &$this, 'enqueue_admin_scripts' ) );
        // Enqueue frontend/editor scripts.
        add_action( 'enqueue_block_assets', array( &$this, 'enqueue_assets' ) );
        add_action( 'enqueue_block_editor_assets', array( &$this, 'enqueue_block_editor_scripts' ) );
        // $this->js_deps = [ 'wp-element', 'wp-i18n', 'wp-hooks', 'wp-components', 'wp-blocks', 'wp-editor', 'wp-compose' ];
        $this->js_deps = array(
            'wp-plugins',
            'wp-element',
            'wp-edit-post',
            'wp-i18n',
            'wp-api-request',
            'wp-data',
            'wp-hooks',
            'wp-plugins',
            'wp-components',
            'wp-blocks',
            'wp-editor',
            'wp-compose'
        );
    }
    
    /**
     * Enqueue front end and editor JavaScript and CSS assets.
     */
    public function enqueue_assets()
    {
        $simple_sitemap_css = $this->utilities_fw->get_enqueue_version( '/lib/assets/css/simple-sitemap.css', $this->plugin_version );
        wp_enqueue_style(
            'simple-sitemap-css',
            $simple_sitemap_css['uri'],
            array(),
            $simple_sitemap_css['ver']
        );
    }
    
    /**
     * Scripts for plugin settings page only.
     *
     * @param String $hook Page hook name.
     * @return Void
     */
    public function enqueue_admin_scripts( $hook )
    {
        if ( 'toplevel_page_simple-sitemap-menu' !== $hook ) {
            return;
        }
        $ss_settings_css = $this->utilities_fw->get_enqueue_version( '/lib/assets/css/simple-sitemap-admin.css', $this->plugin_version );
        $ss_settings_js = $this->utilities_fw->get_enqueue_version( '/lib/assets/js/simple-sitemap-admin.js', $this->plugin_version );
        wp_enqueue_style(
            'simple-sitemap-settings-css',
            $ss_settings_css['uri'],
            array(),
            $ss_settings_css['ver']
        );
        wp_enqueue_script(
            'simple-sitemap-settings-js',
            $ss_settings_js['uri'],
            array(),
            $ss_settings_js['ver']
        );
    }
    
    /**
     * Add scripts for block editor only.
     **/
    public function enqueue_block_editor_scripts()
    {
        $block_editor_js = $this->utilities_fw->get_enqueue_version( '/lib/block_assets/js/blocks.editor.js', $this->plugin_version );
        $deps = $this->js_deps;
        // Block editor script.
        wp_register_script(
            $this->enq_pfx . '-block-editor-js',
            $block_editor_js['uri'],
            $deps,
            $block_editor_js['ver'],
            true
        );
        $data = array(
            'is_premium'           => ss_fs()->is_premium(),
            'can_use_premium_code' => ss_fs()->can_use_premium_code(),
        );
        wp_localize_script( $this->enq_pfx . '-block-editor-js', $this->plugin_settings_prefix . '_editor_data', $data );
        wp_enqueue_script( $this->enq_pfx . '-block-editor-js' );
        $block_editor_css = $this->utilities_fw->get_enqueue_version( '/lib/assets/css/simple-sitemap-block-editor.css', $this->plugin_version );
        // Block editor styles.
        wp_enqueue_style(
            'simple-sitemap-block-editor-css',
            $block_editor_css['uri'],
            array(),
            $block_editor_css['ver']
        );
    }

}
/* End class definition */
msgid ""
msgstr ""
"Content-Type: text/plain; charset=utf-8\n"
"X-Generator: babel-plugin-makepot\n"

#: lib/src/blocks/_components/server-side-render-x.js:148
msgid "Block rendered as empty."
msgstr ""

#: lib/src/blocks/_components/server-side-render-x.js:154
#. %s: error message describing the problem
msgid "Error loading block: %s"
msgstr ""

#: lib/src/blocks/extend-blocks.js:339
msgid "Sitemap Styles"
msgstr ""

#: lib/src/blocks/extend-blocks.js:429
msgid "Featured Image Settings"
msgstr ""

#: lib/src/blocks/simple-sitemap-group/index.js:30
#: lib/src/blocks/simple-sitemap/index.js:30
msgid "Sitemap"
msgstr ""

#: lib/src/blocks/simple-sitemap-group/index.js:31
msgid "Group"
msgstr ""

#: lib/src/blocks/simple-sitemap-group/index.js:32
#: lib/src/blocks/simple-sitemap/index.js:32
msgid "HTML Sitemap"
msgstr ""

#: lib/src/blocks/simple-sitemap-group/index.js:70
#: lib/src/blocks/simple-sitemap/index.js:151
msgid "General Settings"
msgstr ""

#: lib/src/blocks/simple-sitemap/index.js:218
msgid "Tab Settings"
msgstr ""

#: lib/src/blocks/simple-sitemap/index.js:231
msgid "Page Settings"
msgstr ""

#: lib/src/blocks/simple-sitemap/index.js:31
msgid "Single"
msgstr ""